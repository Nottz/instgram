//imports 
const express = require('express');
const app = express();
var models = require('./models');
var router = require('./router');
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//middleware: use a router
app.use('/',router);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
