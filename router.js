// imports
var express = require('express');
var UsersController = require('./controllers/UsersController')
var AuthController = require('./controllers/AuthController')
var PostsController = require('./controllers/PostsController')


// initiate app
var router = express.Router();

// routes

//AUTH
router.post('/auth/register', AuthController.register)
router.post('/auth/login', AuthController.login)
router.post('/auth/logout', AuthController.logout)

//USERS
router.get('/users', UsersController.index)
router.get('/users/:id', UsersController.getById)
router.delete('/users/:id', UsersController.delete)
router.put('/users/:id', UsersController.updateUser)
router.patch('/users/:id', UsersController.updateInfoUser)
router.post('/users/:id', UsersController.create)
router.get('/users/:id/posts', UsersController.getUserPosts)

//POSTS
router.get('/posts', PostsController.getAll)
router.get('/posts/:id', PostsController.getById)
router.post('/posts', PostsController.create)
router.put('/posts/:id', PostsController.updatePost)
router.patch('/posts/:id', PostsController.updateInfoPost)
router.delete('/posts/:id', PostsController.delete)

module.exports = router;