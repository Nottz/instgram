'use strict';

var bcrypt = require('bcrypt')

module.exports = {
  up: (queryInterface, Sequelize) => {

    var hashedPassword = bcrypt.hashSync('johndoe', 12);
    
    var people = []; 
    for(var i = 0; i < 15; i++ ){ 
      people.push({ 
        name: "John Doe"+ i +"", 
        email:"john.doe."+ i +"@yopmail.com", 
        password: hashedPassword, 
        createdAt: new Date(), 
        updatedAt: new Date() 
      }) 
    } 
  return queryInterface.bulkInsert('Users', people, {});
  },

  down: (queryInterface, Sequelize) => {
    
    return queryInterface.bulkDelete('Users', null, {});
    
  }
};
