'use strict';

var models = require('../models');

var randomSentence = require('random-sentence');

module.exports = {
  up: (queryInterface, Sequelize) => {
    console.log('erererererererer')
    return models.User.findAll().then(users => {
      let posts = []
      users.forEach(user => {
        let i = 200+user.id
        let picture = `https://picsum.photos/${i}`
        posts.push({
          userId: user.id,
          title: randomSentence({min: 1, max: 7}),
          desc: randomSentence({min: 10, max: 20}),
          image: picture,
          createdAt: new Date(),
          updatedAt: new Date()
          })
      });
      return queryInterface.bulkInsert('Posts', posts, {});
    })
  },

  down: (queryInterface, Sequelize) => {
    
    return queryInterface.bulkDelete('Posts', null, {});
    
  }
};
