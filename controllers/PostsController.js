var models = require('../models');

/**
 * Controller des posts
 */

class PostsController {

    /**
     * Liste tous les posts
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static getAll(req, res) {
        models.Post.findAll({ include: [models.User]}).then(posts => {
            res.status(200).send(posts);
        })
            .catch( error => {
                res.status(500).json({
                    message: 'Unable to get all the posts',
                    error: error
                });
            });
    }

    /**
     * Récupère un seul post
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static getById(req, res) {
        models.Post.findById(id).then( post => {
            res.status(200).send(post)
        })
            .catch(error => {
                res.status(500).json({
                    message: 'Unable to get this post',
                    error: error
                });
            });
    }

    /**
     * Supprimme un post
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static delete(req, res) {
        models.Post.destroy({where: {id: req.params.id}}).then( post => {
            res.status(204).send();
        }).catch(error => {
            res.status(500).json({
                message: 'Unable to delete the post',
                error: error
            });
        });
    }

    /**
     * Crée un post
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static create(req, res) {

        const title = req.body.title;
        const desc = req.body.desc;
        const image = req.body.image;
        const userId = req.body.userId;

        models.Post.create({
            title,
            desc,
            image,
            userId
        }).then(e =>{
            res.status(200).send({message: "Post bien crée"});
        })

    }

    /**
     * Met à jour la totalité d'un post
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static updatePost(req, res) {
        res.status(200).send({ endpoint: "PostsController.updatePost"});
    }

    /**
     * Met à jour une/plusieurs info(s) d'un post
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static updateInfoPost(req, res) {

        const title = req.body.title;
        const desc = req.body.desc;
        const image = req.body.image;
        const userId = req.body.userId;

        const nweValues = {
            userId,
            title,
            desc,
            image
        }

        models.Post.update(newValues,
            {where: {
                id: req.params.id
            }}
          ).then(function([[updatedPost]]) {
            res.json(updatedPost)
          })
    }
}

module.exports = PostsController;