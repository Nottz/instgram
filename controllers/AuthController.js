var models = require('../models');
var jwt  = require('jsonwebtoken');
var bcrypt = require('bcrypt');
require('dotenv').config();
var cookie = require('cookie-parser');
const expiresIn = 4*60*60 // 4h


/**
 * Controller authentification
 */
class AuthController {


    /**
     * S'inscrire
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static register(req,res) {

        const name = req.body.name;
        const mail = req.body.mail;
        const password = req.body.password;
        const createdAt = new Date();
        const updatedAt = new Date();

        var hashedPassword = bcrypt.hashSync(password, 12);

        models.User.create({
            name,
            mail,
            password: hashedPassword,
            createdAt,
            updatedAt
        })
        .then(e =>{
            res.status(200).send({message: "Profil bien crée"});
        })

    }

    /**
     * Se loger
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static login(req,res) {

        const name = req.body.name;
        const password = req.body.password;

        models.User.findAll({
            where: {
              name
            }
          }).then(data => {
            let user = data[0];
            bcrypt.compare(password, user.password, (err,rese) => {
                if (rese) {
                        const payload = { id : user.id }
                        const token = jwt.sign(payload, process.env.SECRET_TOKEN, { expiresIn: expiresIn })
                        res.cookie("access_token", token)
                        res.status(200).send({
                            message: 'sent'
                        })
                }
                else {
                    res.status(403).send({ 
                        message: "Mauvais logs"
                    });
                }
            })
          });
    }

    /**
     * Se déloguer
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static logout(req, res) {
        res.clearCookie("access_token");
        res.status(200).send({message: "Vous êtes bien déconnecté"});
    }
}

module.exports = AuthController;