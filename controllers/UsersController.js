var models = require('../models');

/**
 * Controller des posts
 */
class UsersController {

    /**
     * Liste tous les users
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static index(req, res) {
        models.User.findAll({
            attributes: {exclude:['password']},
            include:[models.Post]
          }).then(function(users){
            res.send(users)
          })
    }

    /**
     * Affiche un user
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static getById(req, res) {
        res.status(200).send({ endpoint: "UsersController.getById"});
    }

    /**
     * Supprime un user
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static delete(req, res) {
        res.status(200).send({ endpoint: "UsersController.delete"});
    }

    /**
     * Met à jour toutes les données du user
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static updateUser(req, res) {
        res.status(200).send({ endpoint: "UsersController.updateUser"});
    }

    /**
     * Met à jour une/plusieurs info(s) d'un user
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static updateInfoUser(req, res) {
        res.status(200).send({ endpoint: "UsersController.updateInfoUser"});
    }

    /**
     * Crée un user
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static create(req, res) {
        res.status(200).send({ endpoint: "UsersController.create"});
    }

    /**
     * Affiche tous les posts d'un user
     * @param {IncommingMessage} req 
     * @param {ServerResponse} res 
     */
    static getUserPosts(req, res) {
        res.status(200).send({ endpoint: "UsersController.getUserPosts"});
    }
}

module.exports = UsersController;