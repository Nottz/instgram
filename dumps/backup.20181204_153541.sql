-- MySQL dump 10.13  Distrib 8.0.13, for Linux (x86_64)
--
-- Host: localhost    Database: instagram_development
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Posts`
--

DROP TABLE IF EXISTS `Posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `Posts_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Posts`
--

LOCK TABLES `Posts` WRITE;
/*!40000 ALTER TABLE `Posts` DISABLE KEYS */;
INSERT INTO `Posts` VALUES (16,'Hituvuasi judpu erwout.','Dois wowise zune lesdog akver dicope viv nam zuciw gilwaha edepe totok uvene afe vem nifzocem newir.','https://picsum.photos/231',31,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(17,'Wigicjep cohij.','Jilbit heki lepki numfo idijirvu ivdo ci mekig rofegja ji eco jofes tew vepriuw gidac pahlit mujov nok.','https://picsum.photos/232',32,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(18,'La juged baoleciv icawo.','Odijuv roset tedog taf awu riiri micpowcot reda jisef zovif zejep vibmutir liduner.','https://picsum.photos/233',33,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(19,'Focak ipolore kecera atid azico.','Nile copsej sikecre avepepef hovrozte bognen powo pamuhopek tostotisi hidu peg lo sacno kizhi rudo vis.','https://picsum.photos/234',34,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(20,'Sika ri.','Veuke mosvud saihori im cim adizomip so zohi jugezet cuha.','https://picsum.photos/235',35,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(21,'Vug dogogap get vewimom occok haogona.','Echada vo redoj bopfen sokvooh omdo migej dowiwin jeval fobter muk.','https://picsum.photos/236',36,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(22,'Avditpa devi cib ped.','Losicdu voot osugifuf gulec petod guko eldug gi motiale ogipifdot lufiug fonit gobfu picmom noihi haonlow ker.','https://picsum.photos/237',37,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(23,'Zitol wepu.','Wu firigwu papad bujveteh leweto sicef luga ciffumki de veneh dehesem retit dedno.','https://picsum.photos/238',38,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(24,'Hogovcih wovgit irevitut duvere cegnuhkic.','Nirho vonogik wal gespuj fomeuhe apefidubu kilfe huwded cogpoj gutepip iheso dataw ebraziw.','https://picsum.photos/239',39,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(25,'Sama vakpuusi.','Ze ofoli rosti hugojlam solospo ono lorju mebiheg pil paj ecot gi kul usizab.','https://picsum.photos/240',40,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(26,'Zos omti hezop ka delpif zekpe.','Jica enojohte sesun zadsofpal wuef ocana rakte la wilco fisma lokadigi guz downe enu.','https://picsum.photos/241',41,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(27,'Tispe lu cezok.','Cehluv tarzeuz ojis witten suenzen cot fewdoow jiwoh toku declere ze med vobsecko nipi dotbu jihi vahagder ahgis.','https://picsum.photos/242',42,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(28,'Wugrovwo foketo hec rop.','Vitbu bami ziobzuw vu tosi jiw ripo iroih vepilivek eh detrof hacir li edjef as tawesupi igunol.','https://picsum.photos/243',43,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(29,'Diri readibi lol kuwitu je.','Timo nun mehjehi ulugeg saesvi epite zoblo ikovow sojoz mota ci dizew kifcuhhek.','https://picsum.photos/244',44,'2018-12-04 15:21:11','2018-12-04 15:21:11'),(30,'Rojcil upi.','Joju bem tow re isnunkot itpevwut towmep ijoh hapojreb puki lusna sevmaale afbikef kodpudbi fegetif.','https://picsum.photos/245',45,'2018-12-04 15:21:11','2018-12-04 15:21:11');
/*!40000 ALTER TABLE `Posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SequelizeMeta`
--

DROP TABLE IF EXISTS `SequelizeMeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `SequelizeMeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SequelizeMeta`
--

LOCK TABLES `SequelizeMeta` WRITE;
/*!40000 ALTER TABLE `SequelizeMeta` DISABLE KEYS */;
INSERT INTO `SequelizeMeta` VALUES ('20181203151435-create-users.js'),('20181203151635-create-posts.js');
/*!40000 ALTER TABLE `SequelizeMeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (31,'John Doe0','john.doe.0@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(32,'John Doe1','john.doe.1@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(33,'John Doe2','john.doe.2@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(34,'John Doe3','john.doe.3@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(35,'John Doe4','john.doe.4@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(36,'John Doe5','john.doe.5@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(37,'John Doe6','john.doe.6@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(38,'John Doe7','john.doe.7@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(39,'John Doe8','john.doe.8@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(40,'John Doe9','john.doe.9@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(41,'John Doe10','john.doe.10@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(42,'John Doe11','john.doe.11@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(43,'John Doe12','john.doe.12@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(44,'John Doe13','john.doe.13@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11'),(45,'John Doe14','john.doe.14@yopmail.com','$2b$12$fXw.AeQgkaq0AMrLtFMRbOMNMREMjY..P4xvNoXfshFueO8SKAmui','2018-12-04 15:21:11','2018-12-04 15:21:11');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-04 15:35:44
